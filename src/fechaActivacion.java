import java.text.SimpleDateFormat;
import java.util.Date;

public class fechaActivacion implements Rule{
    public String execute(Transaccin transaccion) {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        try{
            Date fechaMinima= formato.parse("2022-01-01");
            if (transaccion.cuenta.fechaActiva.before(fechaMinima)) {

                return "No supera la fecha de activacion";

            }
        }catch (Exception e){
            return "";
        }

        return "";
    }
}
