import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Cuenta cuentaSantiago = new Cuenta();
        cuentaSantiago.usuario = "Santiago";
        cuentaSantiago.activa = false;
        cuentaSantiago.Tpo = "AH";
        cuentaSantiago.monto =10.0;

        Transaccin pagoIcetex = new Transaccin();
        pagoIcetex.fecha = new Date();
        pagoIcetex.montoMovimiento = 0.0;
        pagoIcetex.historico = new ArrayList<>();
        pagoIcetex.cuenta = cuentaSantiago;
        List<Rule> regla= new ArrayList<>();


        Validator validador =new Validator(regla);
        List<String> reglasVioladas = validador.validarTransaccion(pagoIcetex,cuentaSantiago);
        System.out.println(reglasVioladas.stream().collect(Collectors.joining(",")));







    }
    private List<Rule> basicConfig(){
        List<Rule> reglas = new ArrayList<>();
        reglas.add(ruleFactory.generador("FECHA_ACTIVACION"));
        return reglas;
    }
    private List<Rule> medium(){
        List<Rule> reglas = new ArrayList<>();
        reglas.add(ruleFactory.generador("MONTO_MAXIMO"));
        reglas.add(ruleFactory.generador("FECHA_ACTIVACION"));

        return reglas;
    }
    private List<Rule> full(){
        List<Rule> reglas = new ArrayList<>();
        reglas.add(ruleFactory.generador("MONTO_MAXIMO"));
        reglas.add(ruleFactory.generador("SALDO_SUFICIENTE"));
        reglas.add(ruleFactory.generador("FECHA_ACTIVACION"));
        return reglas;
    }
}