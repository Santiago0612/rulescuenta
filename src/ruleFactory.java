public class ruleFactory {
    public static Rule generador(String ruleName){
        if("CUENTA_ACTIVA".equals(ruleName)){
            return new reglaCuentaActiva();
        }
        if("SALDO_SUFICIENTE".equals(ruleName)){
            return new reglaSaldoSuficiente();

        }
        if("MONTO_MAXIMO".equals(ruleName)){
            return new reglaMontoMaximo();
        }
        if("FECHA_ACTIVACION".equals(ruleName)){
            return new fechaActivacion();
        }
        return new reglaCuentaActiva();
    }
}
