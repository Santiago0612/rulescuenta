import java.text.SimpleDateFormat;
import java.util.Date;

public class reglaEntre implements Rule{

    public String execute(Transaccin transaccion) {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        try{
            Date fechaMinima= formato.parse("2022-01-01");
            Date fechaMAxima=formato.parse("2022-06-01");
            if (transaccion.cuenta.fechaActiva.before(fechaMinima)||transaccion.cuenta.fechaActiva.after(fechaMAxima)) {

                return "No supera la activacion";

            }
        }catch (Exception e){
            return "";
        }

        return "";
    }
}
