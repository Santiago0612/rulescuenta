public class reglaSaldoSuficiente implements Rule {

    @Override
    public String execute(Transaccin transaccion) {
        if (transaccion.cuenta.monto < transaccion.montoMovimiento) {

            return "Saldo insuficiente";

        }
        return "";
    }

}
